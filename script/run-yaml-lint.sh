#!/bin/sh

FILENAMES=$(find . \( -name '*.yaml' -o -name '*.yml' \)    \
                    -not -path './node_modules/*'           \
                    -not -path './falco/falco_rules.yaml') # excluding because the file is maintained in the Falco project

yamllint -f colored $FILENAMES

# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "hashicorp/bionic64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  config.vm.network "forwarded_port", guest: 3000, host: 3000, host_ip: "127.0.0.1"
  config.vm.network "forwarded_port", guest: 5060, host: 5060, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.
  config.vm.provider "virtualbox" do |v|
    v.memory = "2048"
    v.cpus = 2
  end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # https://nodejs.org/dist/v16.15.0/node-v16.15.0-linux-x64.tar.xz
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update -y
    # Install base packages.
    apt-get -y install dkms build-essential linux-headers-$(uname -r) apt-transport-https ca-certificates curl gnupg lsb-release

    # Register apt repositories for Falco and Docker.
    curl -s https://falco.org/repo/falcosecurity-3672BA8F.asc | apt-key add -
    echo "deb https://download.falco.org/packages/deb stable main" | tee -a /etc/apt/sources.list.d/falcosecurity.list
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    # Install Docker and Falco.
    apt-get -y update
    apt-get -y install docker-ce docker-ce-cli containerd.io falco=0.29.0

    # Install Node v16.
    wget https://nodejs.org/dist/v16.16.0/node-v16.16.0-linux-x64.tar.xz
    tar -C /usr/local --strip-components 1 -xJvf node-v16.16.0-linux-x64.tar.xz
    rm node-v16.16.0-linux-x64.tar.xz

    # Add user vagrant to docker group to allow it to manage Docker.
    usermod -aG docker vagrant

    # Generate certificates for Falco.
    ## Create a RANDFILE for the root user.
    openssl rand -writerand /root/.rnd
    ## Generate valid CA.
    openssl genrsa -passout pass:1234 -des3 -out ca.key 4096
    openssl req -passin pass:1234 -new -x509 -days 365 -key ca.key -out ca.crt -subj  "/C=SP/ST=Italy/L=Ornavasso/O=Test/OU=Test/CN=Root CA"
    ## Generate valid Server Key/Cert.
    openssl genrsa -passout pass:1234 -des3 -out server.key 4096
    openssl req -passin pass:1234 -new -key server.key -out server.csr -subj  "/C=SP/ST=Italy/L=Ornavasso/O=Test/OU=Server/CN=localhost"
    openssl x509 -req -passin pass:1234 -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt
    ## Remove passphrase from the Server Key.
    openssl rsa -passin pass:1234 -in server.key -out server.key
    ## Generate valid Client Key/Cert.
    openssl genrsa -passout pass:1234 -des3 -out client.key 4096
    openssl req -passin pass:1234 -new -key client.key -out client.csr -subj  "/C=SP/ST=Italy/L=Ornavasso/O=Test/OU=Client/CN=localhost"
    openssl x509 -passin pass:1234 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt
    ## Remove passphrase from Client Key.
    openssl rsa -passin pass:1234 -in client.key -out client.key
    ## Move files to correct location.
    mkdir /etc/falco/certs
    mv server.key /etc/falco/certs/
    mv server.crt /etc/falco/certs/
    mv ca.crt /etc/falco/certs/
    mv client.key /etc/falco/certs/
    mv client.crt /etc/falco/certs/
    mv client.csr /etc/falco/certs/
    ## Make files world-readable.
    chmod +r /etc/falco/certs/*

    # Move Falco configuration file to correct location.
    cp /vagrant/falco/falco.yaml /etc/falco/

    # Install Falco driver
    falco-driver-loader

    # Start Falco
    service falco start

    # Load Falco on every boot.
    ## Load the kernel module/driver automatically on boot.
    echo "falco" >> /etc/modules
    depmod
    ## Start the Falco service automatically on boot
    systemctl enable --now falco
  SHELL

end

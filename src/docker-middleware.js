'use strict'

const path = require('path')
const debug = require('debug')('pkgs:docker-middleware')

const DockerManager = require('./docker-manager.js')

module.exports = function (store, pendingContainer, opts) {
  if (!store) { throw new Error('Required parameter store is missing') }
  if (!pendingContainer) { throw new Error('Required parameter pendingContainer is missing') }

  const _toShortId = function (containerId) {
    return containerId.substring(0, 12)
  }

  const _closeEntry = function (id, status = 'finished') {
    if (!store[id]) throw new Error(`no such id ${id}`)
    store[id].status = status
  }

  return async function (req, res) {
    const filename = path.basename(req.tmpPath)
    const dir = path.dirname(req.tmpPath)
    const docker = new DockerManager(dir)
    const containerId = _toShortId(await docker._create(filename, opts.dockerCreateOpts))
    await docker._cpIntoContainer(req.extractDestinationPath + '/.', opts.dockerCpDestinationPath)

    debug(`installing ${filename} in container ${containerId}`)
    pendingContainer[containerId] = req.id

    docker._start(opts).then(() => {
      _closeEntry(req.id)
    }, (err) => {
      debug(err)
      if (err.name === 'JobTimeoutError') _closeEntry(req.id, 'timeout')
    }).then(() => {
      docker.cleanup()
    })

    res.json({
      status: 'ok',
      id: req.id
    })
  }
}

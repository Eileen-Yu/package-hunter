'use strict'

const basicAuth = require('express-basic-auth')
const debug = require('debug')('pkgs:auth-middleware')

const authorizer = require('./authorizer')
const User = require('./user')

module.exports = function (store) {
  if (process.env.NODE_ENV === 'development' && User.getAllUsers(store).length === 0) {
    debug('Running in development mode. Authentication is disabled.')

    return (req, res, next) => next()
  } else {
    const authenticator = basicAuth({
      authorizer: authorizer(store),
      authorizeAsync: true
    })

    return (req, res, next) => authenticator(req, res, next)
  }
}

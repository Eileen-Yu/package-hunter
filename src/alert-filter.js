const Address6 = require('ip-address').Address6

// Docker's default bridge network has a subnet of 172.17.0.0/16
// Containers are assigned IPs in that subnet, 172.17.0.2, 172.17.0.3, etc.
// Below regex matches outgoing connections from an IP in the default bridge network
// Falco reports such coonnections in the format src_ip:src_port->dest_ip:dest_port
const IPv6EgressConnectionRegex = /^::ffff:172\.17\.\d+\.\d+:\d+->([a-f0-9:]+):443$/

class AlertFilter {
  constructor (alert) {
    this.alert = alert
  }

  isDiscardable () {
    return this.isBundlerIPv6()
  }

  isBundlerIPv6 () {
    // Check for required output fields
    if (!this.alert.output_fields['fd.name'] || !this.alert.output_fields['proc.cmdline']) {
      return false
    }

    // is it Bundler installation?
    if (!this.alert.output_fields['proc.cmdline'].startsWith('bundle /usr/local/bin/bundle install')) {
      return false
    }

    // is it an egress IPv6 connection?
    const match = this.alert.output_fields['fd.name'].match(IPv6EgressConnectionRegex)
    if (!match) {
      return false
    }

    // Is the destination address correct?
    const destAddr = new Address6(match[1])
    return (destAddr.isCorrect() && destAddr.address.endsWith(':ffff'))
  }
}

module.exports = AlertFilter

#!/bin/bash

echo "running $(basename $0)"

printenv > /dev/null 2>&1 
env > /dev/null 2>&1 
set > /dev/null 2>&1 # set is a bash function and calls to it are currently not detected by Falco
